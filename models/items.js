const mongoose = require('mongoose');

const itemsSchema = mongoose.Schema({
  title: String,
  description: String,
  images: String,
  price: Number,
  group: String,
  specialOffer: Boolean
});

const Items = mongoose.model('Items', itemsSchema);
module.exports = Items;
