"use strict"
import { combineReducers } from 'redux';

import { booksReducers } from './booksReducers';
import { cartReducers } from './cartReducers';
import { filterReducers } from './filterReducers';

export default combineReducers({
  books: booksReducers,
  cart: cartReducers,
  filter: filterReducers
});
