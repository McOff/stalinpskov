export const filterReducers = (state='Специальное предложение', action) => {
  if (action.type === 'SET_FILTER') {
    return action.payload;
  }
  return state;
}
