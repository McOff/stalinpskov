import React from 'react';
import { connect } from 'react-redux';
import { Modal, Panel, Col, Row, Well, Button, ButtonGroup, Label, Image } from 'react-bootstrap';
import { deleteCartItem, updateCart, getCart } from '../../actions/cartActions';
import InputMask from 'react-input-mask';

class Cart extends React.Component {
  constructor() {
    super();
    this.state = {
      showModal: false
    }
  }

  componentDidMount() {
    this.props.getCart();
  }

  open() {
    this.setState({
      showModal: true
    })
  }

  close() {
    this.setState({
      showModal: false
    })
  }

  onDelete(_id) {
    let booksToDelete = [...this.props.cart];
    let idxToDel = booksToDelete.findIndex( book => book._id === _id);
    let cartAfterDelete = [...booksToDelete.slice(0, idxToDel), ...booksToDelete.slice(idxToDel + 1)];

    this.props.deleteCartItem(cartAfterDelete);
  }

  onIncrement(_id) {
    this.props.updateCart(_id, 1, this.props.cart);
  }

  onDecrement(_id, qnt) {
    if (qnt > 1) {
      this.props.updateCart(_id, -1, this.props.cart);
    }
  }

  render() {
    if(this.props.cart[0]) {
      return this.renderCart();
    } else {
      return this.renderEmpty();
    }
  }

  renderCart() {
    const cartItemsList = this.props.cart.map(item => {
      return (
        <Panel key={item._id}>
          <Row>
            <Col xs={12} sm={2}>
              <Image src={item.images} responsive />
            </Col>
            <Col xs={12} sm={3}>
              <h6 className="cart__title">{item.title}</h6>
            </Col>
            <Col xs={12} sm={2}>
              <h6 className="cart__price">Цена: <span>{item.price} руб.</span></h6>
            </Col>
            <Col xs={12} sm={3}>
              <h6 className="cart__qty">
                Кол-во:
                <span>
                  <Button className="cart__btn-qty" onClick={this.onDecrement.bind(this, item._id, item.quantity)} bsStyle="default" bsSize="small">-</Button>
                  {item.quantity}
                  <Button className="cart__btn-qty" onClick={this.onIncrement.bind(this, item._id)} bsStyle="default" bsSize="small">+</Button>
                </span>
              </h6>
            </Col>
            <Col xs={12} sm={2}>
              <Button onClick={this.onDelete.bind(this, item._id)} onClick={this.onDelete.bind(this, item._id)} bsStyle="danger" bsSize="small">УДАЛИТЬ</Button>
            </Col>
          </Row>
        </Panel>
      );
    }, this);

    return (
      <div className="cart">
        {cartItemsList}
        <Row>
          <Col xs={12}>
            <div className="cart__order">
              <h6 className="cart__sum">Сумма: <span>{this.props.totalAmount} руб.</span></h6>

              <label>
                <span className="cart__phone-label">
                  Введите Ваш номер телефона:
                </span>
                <InputMask className="cart__phone-form" mask="+7 (999) 999-99-99" maskChar="_" alwaysShowMask={true} />
              </label>

              <button onClick={this.open.bind(this)} className="cart__btn-order">
                Оформить<br/>заказ
              </button>
            </div>
          </Col>
        </Row>
        <Modal show={this.state.showModal} onHide={this.close.bind(this)}>
          <Modal.Header closeButton>
            <Modal.Title>Спасибо!</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <h6>Ваш заказ принят</h6>
            <p>Наш менеджер савяжется с Вами в ближайшее время.</p>
          </Modal.Body>
        </Modal>
      </div>
    )
  }

  renderEmpty() {
    return(<div></div>);
  }
}

function mapStateToProps(state) {
  return {
    cart: state.cart.cart,
    totalAmount: state.cart.totalAmount
  }
}

export default connect(mapStateToProps, { deleteCartItem, updateCart, getCart })(Cart);
