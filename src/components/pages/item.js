import React from 'react';
import { Well, Button, Image } from 'react-bootstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { addToCart, updateCart } from '../../actions/cartActions';
import { Link } from 'react-router';

class BookItem extends React.Component{
  constructor() {
    super();
    this.state = {
      inCart: false
    }
  }

  isInCart() {
    return this.props.cart.forEach(item => {
      if (item._id === this.props._id) {
        this.setState({ inCart: true })
      }

    })
  }

  componentDidUpdate() {
    if (!this.state.inCart) {
      this.isInCart()
    }
  }

  componentDidMount() {
    if (!this.state.inCart) {
      this.isInCart()
    }
  }

  handleCart() {
    const { _id, title, description, price, images } = this.props;
    const book = [ ...this.props.cart, {
      _id,
      title,
      description,
      images,
      price,
      quantity: 1
    }];
    // CHECK IF THE CART IS EMPTY
    if (this.props.cart.length > 0) {
      let bookIndex = this.props.cart.findIndex(book => book._id === _id);

      bookIndex < 0 ? this.props.addToCart(book) : this.props.updateCart(_id, 1, this.props.cart);

    } else {
      this.props.addToCart(book);
    }

  }

  render() {
    return (
      <div className="item">
            <Image src={this.props.images} responsive className="item__img"/>

            <h6 className="item__title">{this.props.title}</h6>
            {/* <p className="item__description">
              {this.props.description}
            </p> */}
            <h6 className="item__price">Цена: <span>{this.props.price} руб.</span></h6>
            { !this.state.inCart ?
                ( <button
                  onClick={this.handleCart.bind(this)}
                  className="item__btn item__btn--red">
                    Добавить<br/>в корзину
                  </button> ) :
                ( <Link to='/cart'>
                    <button
                      onClick={this.handleCart.bind(this)}
                      className="item__btn item__btn--blue">
                        Перейти<br/>в корзину
                      </button>
                  </Link> )
            }

            <div className='item__badge'>
              {
                (!this.props.specialOffer) ?
                  ("") :
                  (<div className='item__action-badge'>
                    Акция
                  </div>)
              }
              {
                (!this.state.inCart) ?
                  ("") :
                  (<div className='item__cart-badge'>
                    В корзине
                  </div>)
              }
            </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    cart: state.cart.cart,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    addToCart, updateCart
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(BookItem);
