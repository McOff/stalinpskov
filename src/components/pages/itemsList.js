import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Carousel, Grid, Row, Button, Col } from 'react-bootstrap';


import Item from './item';
import Cart from './cart';
import { getBooks } from '../../actions/booksActions';

class ItemsList extends React.Component {
  componentDidMount() {
    this.props.getBooks();
  }
  render() {
    const booksList = this.props.books
      .filter(item => {
        if (this.props.filter === "Специальное предложение") {
          return item.specialOffer;
        } else {
          return item.group === this.props.filter;
        }
      })
      .map(function(book) {
        return (
          <Col xs={12} sm={6} md={4} key={book._id}>
            <Item
              _id={book._id}
              title={book.title}
              description={book.description}
              images={book.images}
              price={book.price}
              specialOffer={book.specialOffer}
            />
          </Col>
        )
      })

    return(
      <Grid>
        <Row style={{marginTop: '15px'}}>
          <h2 className="content__heading">{this.props.filter}</h2>
        </Row>
        <Row >
          {booksList}
        </Row>
      </Grid>
    )
  }
}

function mapStateToProps(state) {
  return {
    books: state.books.books,
    filter: state.filter
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getBooks
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ItemsList);
