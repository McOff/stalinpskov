import React from 'react';
import { Grid, Row, Col } from 'react-bootstrap';
import { connect } from 'react-redux';

import { getCart } from '../actions/cartActions';

// import Menu from './menu';
import Hero from './common/hero';
import Footer from './common/footer';
import Sidebar from './common/sidebar';
import StickyHeader from './common/stickyHeader';

class Main extends React.Component {
  componentDidMount() {
    this.props.getCart();
  }

  render() {
    return (
      <div>
        <Hero cartItemsNumber={this.props.totalQty}/>
        <StickyHeader cartItemsNumber={this.props.totalQty}/>
        <Grid>
          <Row>
            <Col sm={3}>
              <Sidebar />
            </Col>
            <Col sm={9}>
              <div className="content">
                {this.props.children}
              </div>
            </Col>
          </Row>
        </Grid>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    totalQty: state.cart.totalQty
  }
}

export default connect(mapStateToProps, { getCart })(Main);
