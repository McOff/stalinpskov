import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';

import { setFilter } from '../../actions/filterActions';

class Sidebar extends React.Component {

  renderItems() {
    let groups = this.props.items.map(item => item.group);
    groups = new Set(groups);
    groups = [...groups];

    return groups.map((group, idx) => {
      let matched = this.props.filter === group;
      return (
        <Link to='/' key={idx}>
          <h3
            className={(matched) ? ("sidebar__group active") : ("sidebar__group")}
            onClick={this.handleClick.bind(this, group)}>
              {group}
          </h3>
        </Link>
      )
    })
  }

  handleClick(filter) {
    this.props.setFilter(filter);
  }

  render() {
    let matched = this.props.filter === "Специальное предложение";

    return (
      <div className="sidebar">
        <h2 className="sidebar__header">Каталог</h2>
        <Link to='/'>
          <h3
            onClick={this.handleClick.bind(this, "Специальное предложение")}
            className={(matched) ? ("sidebar__special-offer active") : ("sidebar__special-offer")}>
            Специальное предложение
          </h3>
        </Link>
        { this.renderItems() }
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    items: state.books.books,
    filter: state.filter
  }
}

export default connect(mapStateToProps, { setFilter })(Sidebar);
