import React from 'react';
import { Grid, Row, Image, Badge } from 'react-bootstrap';
import { Link } from 'react-router';

class StickyHeader extends React.Component {
  constructor() {
    super();
    this.state = {
      visible: false
    };
  }

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll.bind(this));
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll.bind(this));
  }

  handleScroll(event) {
    let scrollTop = event.target.body.scrollTop;

    if (scrollTop > 100) {
      this.setState({
        visible: true
      });
    }

    if (scrollTop < 100) {
      this.setState({
        visible: false
      });
    }
  }

  render(){
    return (
      <div className={this.state.visible ? "sticky" : "sticky--hidden"}>
        <img className="sticky__logo" src='images/smallLogoSite4.png'/>

        <p className="sticky__address">г.Псков, ул. Инженерная, д. 1А<br/>тел. (8112) 61-04-54</p>

        <div className="hero__icons">
          <Link to='/cart'>
            <div className="hero__cart">
              <Image src="images/cart.png" className="hero__icons-cart"/>
              { (this.props.cartItemsNumber) > 0 ?
                <Badge className="badge">{this.props.cartItemsNumber}</Badge> :
                ''
              }
            </div>
          </Link>
        </div>
      </div>
    )
  }
}

export default StickyHeader;
