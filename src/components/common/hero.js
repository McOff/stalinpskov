import React from 'react';
import { Grid, Row, Image, Badge } from 'react-bootstrap';
import { Link } from 'react-router';

class Hero extends React.Component {
  render() {
    return (
      <Grid className="hero">
        <Row>
          <Image src="images/hero.jpg" responsive />
          <div className="hero__icons">
            <a href='https://vk.com/stalinpskov' target="_blank">
              <Image src="images/vk.png" className="hero__icons-vk"/>
            </a>
            <Link to='/cart'>
              <div className="hero__cart">
                <Image src="images/cart.png" className="hero__icons-cart"/>
                { (this.props.cartItemsNumber) > 0 ?
                  <Badge className="badge">{this.props.cartItemsNumber}</Badge> :
                  ''
                }
              </div>
            </Link>
          </div>
        </Row>
      </Grid>
    )
  }
}

export default Hero;
