"use strict"
import React from 'react';
import { render } from 'react-dom';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';

import ItemsList from './components/pages/itemsList';
import Cart from './components/pages/cart';
import ItemsForm from './components/pages/itemsForm';
import Main from './components/main';

const routes = (
      <Router onUpdate={() => window.scrollTo(0, 0)} history={browserHistory}>
        <Route path="/" component={Main}>
          <IndexRoute component={ItemsList} />
          <Route path="/admin" component={ItemsForm} />
          <Route path="/cart" component={Cart} />
        </Route>
      </Router>
);

export default routes;
